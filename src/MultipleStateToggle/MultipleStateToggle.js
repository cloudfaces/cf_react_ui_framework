import React from 'react';
import styles from './MultipleStateToggle.scss';

const StateToggle = (props) => (
  <div>
    <button onClick={() => { props.onClick(props.id) } } id={props.id} className={props.selected ? 'active' : ''}>{props.children}</button>
  </div>
);

class MultipleStateToggle extends React.Component {


  static propTypes = {
    onChange: React.PropTypes.func,
    default: React.PropTypes.string
    // 'selected': React.propTypes.bool,document.setAttribute('attr', value);
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: props.default ? props.default : ""
    }
    this.selectState = this.selectState.bind(this);
  }


  selectState(state) {
    if (this.props.onChange) {
      this.props.onChange(state);
    }
    this.setState({ selected: state });
  }

  render = () => (
    <div className="elipse-holder">
      <StateToggle onClick={this.selectState} selected={this.state.selected == "on"} id="on">ON</StateToggle>
      <StateToggle onClick={this.selectState} selected={this.state.selected == "off"} id="off">OFF</StateToggle>
      <StateToggle onClick={this.selectState} selected={this.state.selected == "eco"} id="eco">ECO</StateToggle>
    </div>
  );

}

export default MultipleStateToggle;
