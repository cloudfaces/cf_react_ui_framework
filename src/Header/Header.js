import React from 'react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './Header.scss';

class Header extends React.Component {
	static defaultProps = {
	    className: '',
	    children: '',
	    style: {}
	};

	static propTypes = {
		style: PropTypes.object
	};

 	render(){
 		return (
 			<Row>
	 			<Col className="header-container" xs={12}>
	 				<div className={`header ${this.props.className}`} style={this.props.style}>
	 					{this.props.children}
	 				</div>
	 			</Col>
	 		</Row>
 		) 		
 	};

}	

export default Header;
