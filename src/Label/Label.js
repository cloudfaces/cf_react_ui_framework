import React from 'react';
import styles from './Label.scss';

class Label extends React.Component {

  static propTypes = {
    'className': React.PropTypes.string,
    'children': React.PropTypes.any,
    'for': React.PropTypes.string
  };

  constructor(props) {
    super(props);
    this.type = "text";
  }

  render = () => (
    <label className={this.props.className} htmlFor={this.props.for}> {this.props.children}</label>
  );


}

export default Label;
