import React from 'react';
import styles from './Slider.scss';
import Nouislider from 'react-nouislider';

class Slider extends React.Component {
  static propTypes = {
    'range': React.PropTypes.object,
    'metrics': React
      .PropTypes
      .oneOf(["C", "F"])
  };

  constructor(props) {
    super(props);
  }

  convert2F(value) {
    return value * 1.8 + 32;
  }

  render = () => (

    <div className="set-height">
      <div className="range">
        {this.props.metrics === 'F'
          ? this.convert2F(this.props.range.max)
          : this.props.range.max}
        °
      </div>
      <Nouislider {...this.props}/>
      <div className="range">
        {this.props.metrics === 'F'
          ? this.convert2F(this.props.range.min)
          : this.props.range.min}
        °
      </div>
    </div>
  )
}

export default Slider;
