/* global CFNativeComponent*/
import React from 'react';
import { Button as ButtonBootstrap } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends React.Component {
    static defaultProps = {
        className: '',
        children: '',
        id: '',
        style: {},
        type: 'button'
    };

    static propTypes = {
        style: PropTypes.object,
        'type': React.PropTypes.string,
        'onTouchTap': React.PropTypes.func,
        'disabled': React.PropTypes.bool,
    };

    render(){
        return (
            <ButtonBootstrap 
                {...this.props}
                id={this.props.id} 
                style={this.props.style}
                type={this.props.type}
                onClick={() => {(!this.props.disabled && this.type == 'button') && this
                      .props
                      .onTouchTap()
                }}        
            >
                {this.props.children}
            </ButtonBootstrap>
        )       
    };

}   

export default Button;
