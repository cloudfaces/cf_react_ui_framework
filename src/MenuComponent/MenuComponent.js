import React from 'react';
import styles from './MenuComponent.scss';

class MenuComponent extends React.Component {


  static propTypes = {

  };

  constructor (props) {
        super(props);
  }


 render  = () => (
   <div>
   <ul className="slide_menu menu-list" id="menu_list">
        <li className="active home">
            <a data-lang="slideout_home" className="home default-txt montserrat"><i className="fa fa-home" aria-hidden="true"></i> Control</a>
        </li>                
        <li className="analytics">
            <a data-lang="analytics" className="analytics default-txt montserrat" data-name="analytics">Analytics

            </a>
        </li>
        <li className="profile">
            <a data-lang="profile" className="profile default-txt montserrat" data-name="profile">Profile
            </a>
        </li>
        <li className="notif">
            <a data-lang="notifications" className="notifications default-txt montserrat" data-name="notifications">Notifications
            </a>
        </li> 
        <li className="signout">
            <a id="logout_submit" data-lang="signout" className="signout default-txt montserrat" data-name="signout">Log out
            </a>
        </li>                                       
    </ul>
  </div>
 );

}
export default MenuComponent;
