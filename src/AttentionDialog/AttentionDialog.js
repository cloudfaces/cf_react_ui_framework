import React from 'react';
import styles from './AttentionDialog.scss';
import TextComp from '../TextComp';

class AttentionDialog extends React.Component {
  static propTypes = {
    shown: React.PropTypes.bool,
    text: React.PropTypes.string
  }
  render = () => (
    <div
      className="attention-dialog"
      style={{
      visibility: this.props.shown
        ? "visible"
        : "hidden"
    }}>
    <img src={require('./img/attention-icon.png')}/>
    <TextComp className="attention-text">{this.props.text}</TextComp>
     
    </div>
  )
}

export default AttentionDialog;
