import React from 'react';
import styles from './Ribbon.scss';

class Ribbon extends React.Component {


 static propTypes = {
   'children' : React.PropTypes.array,
 };
 
 constructor (props) {
       super(props);
 }

 render  = () => (
    <div className="ribbon">
        <img className="pull-left" src={require('./img/home-temperature-icon.png')}/>
        {this.props.children}
        <img className="pull-right img-responsive" src={require('./img/electricity.png')}/>
    </div>
 );

}

export default Ribbon;
