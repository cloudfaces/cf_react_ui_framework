import React from 'react';
import styles from './TextComp.scss';
import classNames from 'classnames/bind';
class TextComp extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.className);
  }

  getClass(name) {
    let textType = this.props.type;
    return textType;
  }
 
  render = () => {
    let classAttr = 'header-primary ' + (this.props.type ? this.className : "");
    return (
      <p
        className={classAttr}
        type="header-primary"
      >
        {this.props.children}
      </p>
    )
  }
}

export default TextComp;
