/* global CFNavigation CFMenuNavigation */

import React from 'react';
import styles from './Link.css';

class Link extends React.Component{
    constructor(props){
        super(props);
        this.navigate = this.navigate.bind(this);
    }

    navigate(id, context){
        const {type} = this.props;
        if (type == 'back') {
                CFNavigation.navigateBack(null);
        } else if(id) {
                CFNavigation.navigate(id, context);
        }        
    }

    render = () => {
        return(
            <a className={this.props.className} onTouchTap={()=>{this.navigate(this.props.target, this.props.context)}}>{this.props.children}</a>
        );
    }
}

export default Link;
