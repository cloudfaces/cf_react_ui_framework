import React from 'react';
import styles from './ModalDialog.scss';
import Header from '../Header';

class ModalDialog extends React.Component {
  static propTypes = {
    shown: React.PropTypes.bool,
    header: React.PropTypes.string,
    modalContent: React.PropTypes.object
  }
  render = () => (
    <div
      className="modal"
      style={{
      visibility: this.props.shown
        ? "visible"
        : "hidden"
    }}>
      <div className="modal-content">
        <Header type="grey">{this.props.header}</Header>
        {this.props.modalContent}
      </div>
    </div>
  )
}

export default ModalDialog;
