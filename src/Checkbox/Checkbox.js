import React from 'react';
import './Checkbox.scss';
import classNames from 'classnames/bind';
import Label from '../Label';

class Checkbox extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    'onClick': React.PropTypes.func,
    'disabled': React.PropTypes.bool,
    'checked': React.PropTypes.number,
    'feedback': React.PropTypes.bool,
    'children': React.PropTypes.string,
    'className': React.PropTypes.string,
    'id': React.PropTypes.string
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.type)
  }

  getClass(name) {
    let toggleType = this.props.type;

    return toggleType;
  }

  render = () => (

    <div className={this.props.inline ? "checkbox-inline" : ""}>
      <input
      checked={this.props.checked}
        onChange={this.props.onChange}
        value={this.props.value}
        className={"checkbox " + this.className}
        type="checkbox"
        id={this.props.id}/>
      <label htmlFor={this.className} className="check-label">{this.props.children}</label>

    </div>
  )
}

export default Checkbox;
