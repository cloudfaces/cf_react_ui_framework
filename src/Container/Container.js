import React from 'react';
import { Grid } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './Container.scss';

class Container extends React.Component {
	static defaultProps = {
	    className: '',
	    children: '',
	    id: '',
	    style: {}
	};

	static propTypes = {
		style: PropTypes.object
	};

 	render(){
 		return (
 			<Grid id={this.props.id} className={`${this.props.className}`} style={this.props.style}>
				{this.props.children}
			</Grid>
 		) 		
 	};

}	

export default Container;
