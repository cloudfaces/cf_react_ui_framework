import React from 'react';
import './ToggleOnOff.scss';
import Label from '../Label';

class ToggleOnOff extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    'onClick': React.PropTypes.func,
    'disabled': React.PropTypes.bool,
    'checked': React.PropTypes.any,
    'feedback': React.PropTypes.bool,
    'className': React.PropTypes.string
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.type)
  }

  getClass() {
    let toggleType = this.props.type;
    return toggleType;
  }

  render = () => (
      <div id="onOff" className={this.props.stateToggle ? this.props.temp ? "switch temp-toggle" : "switch state-toggle" : "switch onOff"}  onChange={this.props.onChange}>
        <Label>
          <input
            defaultChecked={this.props.checked}
            className="checkbox-toggle melissa-toggle"
            type="checkbox"/>
          <div className="togg round"/>
        </Label>
      </div>
  )
}

export default ToggleOnOff;
