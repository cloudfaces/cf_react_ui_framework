import React from 'react';
import './MobileDebuger.scss';


class MobileDebuger extends React.Component {
  static propTypes = {

  }
  render = () => (
    <div id="mobile-debuger">
      <TabBar />
      <Content>
        {this.props.children}
      </Content>
    </div>
  )
}

class TabBar extends React.Component {
  static propTypes = {

  }
  render = () => (
    <div id="mobile-tab-bar">
      
    </div>
  )
}

class Content extends React.Component {
  static propTypes = {

  }
  render = () => (
    <div id="mobile-content">
      {this.props.children}
    </div>
  )
}


export default MobileDebuger;
