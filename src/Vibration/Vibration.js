/* global CFNativeComponent*/

import React from 'react';

class Vibration extends React.Component {
  static propTypes = {
    vibrate: React.PropTypes.bool
  }
  
    
  vibrate(){
     const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    if (this.props.vibrations) {
        if (navigator.vibrate) {
            console.log("its vibrating");
            window.navigator.vibrate(50); 
        } else if (iOS == true) {
            CFNativeComponent.vibratePhone('5');
        }
    }
  }


  render = () => (
    <div onClick={()=>{this.vibrate()}}>{this.props.children}</div>
  )
}

export default Vibration;
