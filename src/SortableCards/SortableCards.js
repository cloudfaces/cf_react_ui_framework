import React from 'react';
import styles from './SortableCards.scss';
import {SortableContainer, SortableElement} from 'react-sortable-hoc'; 
import DeviceCard from '../DeviceCard';
const SortableItem = SortableElement(({value}) =>
  <DeviceCard/>
);

class SortableCards extends React.Component {
  state = {
    items: []
  };

  render() {
    return <SortableList axis="xy" items={[1,2,3,4, 5]} onSortEnd={this.onSortEnd} />;
  }
}


export default SortableCards;
