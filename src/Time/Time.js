import React from 'react';
import styles from './Time.scss';
import classNames from 'classnames/bind';

class Time extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    'className': React.PropTypes.string
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.type)
  }

  getClass(name) {
    let TimeType = this.props.type;

    return TimeType;
  }

  render = () => (
    <div className="smart-option">
      <div className="option-text translated-time">Time:</div>
      <div id="ac-timer-settings">
        <input className={"time " + this.className} type="time"/>
      </div>
    </div>
  )
}

export default Time;
