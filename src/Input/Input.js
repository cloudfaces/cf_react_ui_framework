import React from 'react';
import styles from './Input.scss';

class Input extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    'disabled': React.PropTypes.bool,
    'minLength': React.PropTypes.string,
    'required': React.PropTypes.bool,
    'placeholder': React.PropTypes.string,
    'id': React.PropTypes.string,
    'className': React.PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.type = "text";
    this.focus = "";
  }

  render = () => {
    let classAttr = 'input-main ' + (this.props.error
      ? "red_borders"
      : "");
    return (<input
      type={this.props.type}
      placeholder={this.props.placeholder}
      disabled={this.props.disabled}
      className={classAttr + this.props.className}
      id={this.props.id}
      value={this.props.value} 
      onChange={this.props.onChange}
      />)
  }
}
export default Input;
