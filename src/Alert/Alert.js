import React from 'react';
import './Alert.scss';

class Alert extends React.Component {
  static propTypes = {
    shown: React.PropTypes.bool,
    message: React.PropTypes.string,
    buttons: React.PropTypes.bool,
    confirm: React.PropTypes.func,
    reject: React.PropTypes.func,
  }
  render = () => (
    <div
      className={this.props.shown ? "popup-alert slide" : "popup-alert"}
      style={{
      visibility: this.props.shown
        ? "visible"
        : "hidden"
    }}>
      <div className="popup-content">
        <span className="message-text">{this.props.message}</span>
        <div className="clear"></div>
        <div className={this.props.buttons ? "confirm show" : "hide"}>
          <button className="btn melissa-primary" onClick={this.props.confirm}>Yes</button>
          <button className="btn melissa-primary" onClick={this.props.reject}>No</button>
        </div>
      </div>
    </div>
  )
}

export default Alert;
