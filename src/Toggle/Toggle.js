import React from 'react';
import './Toggle.scss';
import Label from '../Label';

class Toggle extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
    'onClick': React.PropTypes.func,
    'disabled': React.PropTypes.bool,
    'checked': React.PropTypes.any,
    'feedback': React.PropTypes.bool,
    'className': React.PropTypes.string
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.type)
  }

  getClass(name) {
    let toggleType = this.props.type;
    return toggleType;
  }

  render = () => (
      <div id="generalToggle" className="switch" onChange={this.props.onChange}>
        <Label>
          <input
            checked={this.props.checked}
            className={"checkbox-toggle " + this.className}
            type="checkbox"/>
          <div className="togg round"/>
        </Label>
      </div>
  )
}

export default Toggle;
