import React from 'react';
import styles from './ListItems.scss';

class ListItems extends React.Component {

  static propTypes = {
    'children': React.PropTypes.array,
    'iiAssigned': React.PropTypes.array,
    'className': React.PropTypes.string

  };

 constructor(props) {
    super(props);
    this.state={
      isAssigned: false
    }
  }

  render() {
    let assigned = this.props.className + (this.state.isAssigned
      ? " if-assigned"
      : " ");
    return (
      <ul className="melissa-list">
        {this
          .props
          .list
          .map(function (listValue) {
            return <li className={assigned}>{listValue}
              <p className="assigned-melissa">Already assigned.</p>
              </li>;
          })}
      </ul>
    )
  }

}
export default ListItems;
