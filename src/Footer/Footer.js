import React from 'react';
import styles from './Footer.scss';
import Button from '../Button';


class Footer extends React.Component {


  static propTypes = {
    'children': React.PropTypes.object,
    'type': React.PropTypes.string,
    'onClick': React.PropTypes.func,
    'disabled': React.PropTypes.bool,
    'feedback': React.PropTypes.bool
  };

  constructor (props) {
        super(props);
        this.className = this.getClassFromType(this.props.type);
  }

  getClassFromType(type){
      let className = ' btn btn-default';
      switch (type) {
        case "success":
          className = "btn-success btn"
          break;
        case "primary-fixed":
          className = "btn-fixed btn-primary btn"
          break;
        case "primary":
          className = "btn-primary btn"
          break;
        case "inactive":
          className = "btn-inactive btn"
          break;
        case "connecting":
          className = "btn-connecting btn"
          break;
        default:
          className = "btn btn-primary"
          break;
      }
      return className;
    }

 render  = () => (
   <footer className="fixed-footer">
    {this.props.children}
   </footer>
 );

}
export default Footer;
