import React from 'react';
import styles from './Span.scss';
class Span extends React.Component {
  static propTypes = {
    'type': React.PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.className = this.getClass(this.props.className);
  }

  getClass(name) {
    let textType = this.props.type;
    return textType;
  }

  render = () => {
    let classAttr = 'header-primary ' + (this.props.type ? this.className : "");
    return (
      <span
        className={classAttr}
        type="highlight"
      >
        {this.props.children}
      </span>
    )
  }
}

export default Span;
