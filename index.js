/*Cloudfaces standard elements*/

// import Alert from './src/Alert';
import Button from './src/Button';
import Header from './src/Header';
import Footer from './src/Footer';
import Input from './src/Input';
import Slider from './src/Slider';
import Link from './src/Link';

// import Label from './src/Label';
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import Time from './src/Time';
// import ModalDialog from './src/ModalDialog';
// import AttentionDialog from './src/AttentionDialog';

// import ListItems from './src/ListItems';
// import MenuComponent from './src/MenuComponent';

// import Ribbon from './src/Ribbon';
import TextComp from './src/TextComp';
import Span from './src/Span';
import Title from './src/Title';
import Toggle from './src/Toggle';
// import Checkbox from './src/Checkbox';
// import Radio from './src/Radio';
import Container from './src/Container';
import Page from 'react-layout-components';
import Box from 'react-layout-components';
import ToggleOnOff from './src/ToggleOnOff';

/*React bootstrap*/
export Accordion from 'react-bootstrap/lib/Accordion';
export Alert from 'react-bootstrap/lib/Alert';
export Badge from 'react-bootstrap/lib/Badge';
export Breadcrumb from 'react-bootstrap/lib/Breadcrumb';
export BreadcrumbItem from 'react-bootstrap/lib/BreadcrumbItem';
// export Button from 'react-bootstrap/lib/Button';
export ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
export ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
export Carousel from 'react-bootstrap/lib/Carousel';
export CarouselItem from 'react-bootstrap/lib/CarouselItem';
export Checkbox from 'react-bootstrap/lib/Checkbox';
export Clearfix from 'react-bootstrap/lib/Clearfix';
export CloseButton from 'react-bootstrap/lib/CloseButton';
export ControlLabel from 'react-bootstrap/lib/ControlLabel';
export Col from 'react-bootstrap/lib/Col';
export Collapse from 'react-bootstrap/lib/Collapse';
export Dropdown from 'react-bootstrap/lib/Dropdown';
export DropdownButton from 'react-bootstrap/lib/DropdownButton';
export Fade from 'react-bootstrap/lib/Fade';
export Form from 'react-bootstrap/lib/Form';
export FormControl from 'react-bootstrap/lib/FormControl';
export FormGroup from 'react-bootstrap/lib/FormGroup';
export Glyphicon from 'react-bootstrap/lib/Glyphicon';
export Grid from 'react-bootstrap/lib/Grid';
export HelpBlock from 'react-bootstrap/lib/HelpBlock';
export Image from 'react-bootstrap/lib/Image';
export InputGroup from 'react-bootstrap/lib/InputGroup';
export Jumbotron from 'react-bootstrap/lib/Jumbotron';
export Label from 'react-bootstrap/lib/Label';
export ListGroup from 'react-bootstrap/lib/ListGroup';
export ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
export Media from 'react-bootstrap/lib/Media';
export MenuItem from 'react-bootstrap/lib/MenuItem';
export Modal from 'react-bootstrap/lib/Modal';
export ModalBody from 'react-bootstrap/lib/ModalBody';
export ModalDialog from 'react-bootstrap/lib/ModalDialog';
export ModalFooter from 'react-bootstrap/lib/ModalFooter';
export ModalHeader from 'react-bootstrap/lib/ModalHeader';
export ModalTitle from 'react-bootstrap/lib/ModalTitle';
export Nav from 'react-bootstrap/lib/Nav';
export Navbar from 'react-bootstrap/lib/Navbar';
export NavbarBrand from 'react-bootstrap/lib/NavbarBrand';
export NavDropdown from 'react-bootstrap/lib/NavDropdown';
export NavItem from 'react-bootstrap/lib/NavItem';
export Overlay from 'react-bootstrap/lib/Overlay';
export OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
export PageHeader from 'react-bootstrap/lib/PageHeader';
export PageItem from 'react-bootstrap/lib/PageItem';
export Pager from 'react-bootstrap/lib/Pager';
export Pagination from 'react-bootstrap/lib/Pagination';
export Panel from 'react-bootstrap/lib/Panel';
export PanelGroup from 'react-bootstrap/lib/PanelGroup';
export Popover from 'react-bootstrap/lib/Popover';
export ProgressBar from 'react-bootstrap/lib/ProgressBar';
export Radio from 'react-bootstrap/lib/Radio';
export ResponsiveEmbed from 'react-bootstrap/lib/ResponsiveEmbed';
export Row from 'react-bootstrap/lib/Row';
export SafeAnchor from 'react-bootstrap/lib/SafeAnchor';
export SplitButton from 'react-bootstrap/lib/SplitButton';
export Tab from 'react-bootstrap/lib/Tab';
export TabContainer from 'react-bootstrap/lib/TabContainer';
export TabContent from 'react-bootstrap/lib/TabContent';
export Table from 'react-bootstrap/lib/Table';
export TabPane from 'react-bootstrap/lib/TabPane';
export Tabs from 'react-bootstrap/lib/Tabs';
export Thumbnail from 'react-bootstrap/lib/Thumbnail';
export ToggleButton from 'react-bootstrap/lib/ToggleButton';
export ToggleButtonGroup from 'react-bootstrap/lib/ToggleButtonGroup';
export Tooltip from 'react-bootstrap/lib/Tooltip';
export Well from 'react-bootstrap/lib/Well';

export {
    Container, Box, Page, Button, Header, Footer, Input,
    Title, Toggle, TextComp,
    Time, Slider, Span, Link, 
    ToggleOnOff
}