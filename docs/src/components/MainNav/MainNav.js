import React from 'react';
import cssModule from 'react-css-modules';
import styles from './MainNav.scss';
import {Link, IndexLink} from 'react-router';


const MainNav = () => (
  <div id="main-nav">
    <Link to='/' activeClassName="active"><span className="logo-text">MClimate React UI</span></Link>
    <ul>
      <li><a>Introduction</a></li>
      <li><a>Getting started</a></li>
      <li><Link to='components' activeClassName="active">Components</Link></li>
      {/*<li><Link to='login' activeClassName="active">Login</Link></li>*/}
      {/*<li><Link to='registration' activeClassName="active">Registration</Link></li>*/}
      
    </ul>
    <div className="clearfix"></div>
  </div>
);

MainNav.propTypes = {

};

export default cssModule(MainNav, styles);
