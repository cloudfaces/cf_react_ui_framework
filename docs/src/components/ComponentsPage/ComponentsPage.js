import React from 'react';
import styles from './ComponentsPage.scss';
import MainContainer from '../MainContainer';
import MainNav from '../MainNav';
import Tabs, {TabPane} from 'rc-tabs';
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import TabContent from 'rc-tabs/lib/TabContent';

// import Nouislider from 'react-nouislider';
import {
  Container,
  Box,
  Page,
  Alert,
  Button,
  Header,
  WaterHeaterSlider,
  Footer,
  Input,
  Label,
  MenuComponent,
  Ribbon,
  Title,
  Toggle,
  TextComp,
  Checkbox,
  Time,
  Slider,
  Span,
  DeviceCard,
  AddDevice,
  PowerButton,
  MelissaChooseCard,
  ListItems,
  ModalDialog,
  AttentionDialog,
  ToggleOnOff
} from '../../../../';
/*const SortableItem = SortableElement(({value}) => <DeviceCard/>);
const SortableList = SortableContainer(({items}) => {
  return (
    <div>
      {items.map((value, index) => (<SortableItem key={`item-${index}`} index={index} value={value}/>))}
      <AddDevice/>
    </div>
  );
});*/
let callback = function () {};
class ComponentsPage extends React.Component {

  static propTypes = {}

  constructor(props) {
    super();
    this.state = {
      mode: "cold",
      ACState: false,
      name: "Melissa",
      homeHumidity: 25,
      homeTemperature: 25,
      acTemperature: [25],
      activeIcon: false,
      test: [
        1, 2, 3, 4, 6
      ],
      inputValue: ""
    }

  }

  render = () => (
    <div>
      <Container alignItems="flex-center" column>
        <Title type="center h2">Headers</Title>
        <Box justifyContent="center" flexBasis>
          <Box>
            <Header src={require('./img/find-bobbie.png')} type="center white">Header<TextComp>Sample info under headline.</TextComp>
            </Header>
          </Box>
          <Box>
            <Header src={require('./img/assigned-bobbie.png')}>Header<TextComp>Sample info under headline.</TextComp>
            </Header>
          </Box>
          <Box>
            <Header src={require('./img/find-bobbie.png')} type="center default-txt">Header<TextComp>Sample info under headline.</TextComp>
            </Header>
          </Box>
        </Box>
      </Container>

      <Container column>
        <Title type="center h2">Text and Higlights in text</Title>
        <Box alignItems="flex-center" row>
          <Box flex={5}>
            <TextComp>Lorem Ipsum is simply dummy text of
              <Span type="highlight">Hightlights</Span>
              the printing and typesetting industry. Lorem Ipsum has been the industry's
              standard dummy. Lorem Ipsum is simply dummy text of
              <Span type="highlight">Hightlights</Span>
              the printing and typesetting industry. Lorem Ipsum has been the industry's
              standard dummy</TextComp>
          </Box>
          <Box flex={3}>
            <TextComp>Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy</TextComp>
          </Box>
        </Box>
      </Container>

      <Container alignItems="flex-center" column>
        <Title type="center h2">Tabs</Title>

        <div id="device-list">
          <Tabs
            className="bobbie-tabs"
            defaultActiveKey="1"
            onChange={callback}
            renderTabBar={() => <ScrollableInkTabBar/>}
            renderTabContent={() => <TabContent/>}>
            <TabPane className="online-bobbies" tab="Bobbie (1)" key="1">
              Content
            </TabPane>
            <TabPane className="offline-bobbies" tab="Bobbie (2)" key="2">
              Content
            </TabPane>
          </Tabs>

          <Tabs
            className="melissa-tabs"
            defaultActiveKey="1"
            onChange={callback}
            renderTabBar={() => <ScrollableInkTabBar/>}
            renderTabContent={() => <TabContent/>}>
            <TabPane className="online-bobbies" tab="Melissa (1)" key="1">
              Content
            </TabPane>
            <TabPane className="offline-bobbies" tab="Melissa (2)" key="2">
              Content
            </TabPane>
          </Tabs>
        </div>
      </Container>

      <Container alignItems="flex-center" column>
        <Title type="center h2">Buttons</Title>
          <Button
            onTouchTap={() => {
            console.log('touch test')
          }}
            type="bobbie-primary">B Primary</Button>

        <Button type="melissa-primary">M Primary</Button>
        <Button type="bobbie-default">B Default Button</Button>
        <Button type="melissa-default">M Default Button</Button>
        <Button type="grey">B Grey Button</Button>
        <Button type="success">B Success</Button>
        <Button disabled={true} type="melissa-primary">B Primary Disabled Button</Button>
        <Button type="delete">Delete Button</Button>
        <Button type="connecting">Connecting
          <div className="spinner">
            <div className="bounce1"/>
            <div className="bounce2"/>
            <div className="bounce3"/>
          </div>
        </Button>
        <Button type="transparent">B&M Button</Button>
      </Container>

      <Container alignItems="flex-center" column center>
        <Title type="center h2">Inputs</Title>
        <Input
          type="text"
          className="white-borders"
          disabled={true}
          placeholder="First name"/>
        <Input type="email" placeholder="Email"/>
        <Input type="password" placeholder="Password"/>
        <Input type="tel" placeholder="Phone number" className="phone"/>
        <Box center>
          <Time type="timer"/>
        </Box>
      </Container>

      <Container alignItems="flex-center" column>
        <Title type="center h2">Toggle Buttons</Title>
        <div className="center">
          <Toggle onOffText={true} type="melissa-toggle" onChange={this.toggleState}/>
          <Toggle type="bobbie-toggle" onChange={this.toggleState}/>
          hallo
          <ToggleOnOff/>
          <ToggleOnOff stateToggle={true}/>
        </div>
      </Container>

      <Container alignItems="flex-center" column>
        <Title type="center h2">Checkboxes</Title>
        <div className="center">
          <Container column center>
            <Checkbox type="melissa-checkbox">Melissa</Checkbox>
            <Checkbox type="bobbie-checkbox">Bobbie</Checkbox>
          </Container>
        </div>
      </Container>

      <Container className="test" center column>
        <PowerButton/>
        <Title type="center h2">Sliders</Title>
        <Container center>
          <Slider
            metrics="C"
            range={{
            min: 16,
            max: 30
          }}
            start={this.state.acTemperature}
            step={1}
            orientation="vertical"
            direction='rtl'/>
        </Container>
        <Container center>
          {/*<Slider orientation="horizontal"/>*/}
        </Container>
      </Container>

      <Container className="test">
        {/* <MelissaControls/> */}
      </Container>

      <div>
        <MelissaChooseCard type="basic">Melissa Climate Wi-Fi</MelissaChooseCard>
        <MelissaChooseCard type="combo">Melissa Climate 3G Combo</MelissaChooseCard>
        <MelissaChooseCard type="combo-type1"></MelissaChooseCard>
        <MelissaChooseCard type="combo-type2"></MelissaChooseCard>
        <MelissaChooseCard type="melissa-blinks red">constantly red</MelissaChooseCard>
        <MelissaChooseCard type="melissa-blinks blue">blinks in blue</MelissaChooseCard>
        <MelissaChooseCard type="melissa-blinks light">pulsates in light blue</MelissaChooseCard>
      </div>
      {/* <ListItems className="nearby" list={["Brand", "brand", "Brand"]}/> */}

      <Container className="test">
        <DeviceCard
          deviceName="Bobbie"
          ecoMode={true}
          tempLevel={22}
          homeHumidity={22}
          homeTemperature={25}
          deviceType="melissa"
          updating={true}
          sending={false}/>
        <DeviceCard deviceName="bobbie" deviceType="bobbie" className="offline"/>
      </Container>

      <Container>
        <ModalDialog shown={false}/>
      </Container>

      <Container>
        <AttentionDialog
          text="Do not use hot water during the callibration process."
          shown={true}/>
      </Container>

    </div>

  )
}

export default ComponentsPage;
